// Pre-Loading
setTimeout(function () {
  $('body').addClass('loaded');
}, 2000);
// End
(function ($) {
  "use strict";
  $(function () {
    $("#contact-form").validator();
    $("#contact-form").on("submit", function (e) {
      if (!e.isDefaultPrevented()) {
        var url = "contact_form/contact_form.php";
        $.ajax({
          type: "POST",
          url: url,
          data: $(this).serialize(),
          success: function (data) {
            var messageAlert = "alert-" + data.type;
            var messageText = data.message;
            var alertBox =
              '<div class="alert ' +
              messageAlert +
              ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
              messageText +
              "</div>";
            if (messageAlert && messageText) {
              $("#contact-form").find(".messages").html(alertBox);
              $("#contact-form")[0].reset();
            }
          },
        });
        return false;
      }
    });
  });
  function customScroll() {
    var windowWidth = $(window).width();
    if (windowWidth > 991) {
      $(".pt-page").mCustomScrollbar({ scrollInertia: 8 });
      $("#site_header").mCustomScrollbar({ scrollInertia: 8 });
    } else {
      $(".pt-page").mCustomScrollbar("destroy");
      $("#site_header").mCustomScrollbar("destroy");
    }
  }
  $(window)
    .on("load", function () {
      $(".preloader").fadeOut("slow");
      var ptPage = $(".subpages");
      if (ptPage[0]) {
        PageTransitions.init({ menu: "ul.site-main-menu" });
      }
      customScroll();
    })
    .on("resize", function () {
      customScroll();
    });
})(jQuery);




// Click Content Portfolio
const portfolioBoxes = document.querySelectorAll('.portfolio-box');
const contentContainers = document.querySelectorAll('.content-container');

portfolioBoxes.forEach((box) => {
  box.addEventListener('click', () => {
    const boxTitle = box.dataset.title;
    contentContainers.forEach((container) => {
      if (container.dataset.title === boxTitle) {
        container.style.display = 'block';
      } else {
        container.style.display = 'none';
      }
    });
  });
});

// Hidden content
const closeButtons = document.querySelectorAll('.close-button');

closeButtons.forEach((button) => {
  button.addEventListener('click', () => {
    const contentContainer = button.parentNode;
    contentContainer.style.display = 'none';
  });
});

// Carousel
$('#owl-carousel').owlCarousel({
  loop: true,
  margin: 30,
  dots: true,
  nav: true,
  items: 1,
})


